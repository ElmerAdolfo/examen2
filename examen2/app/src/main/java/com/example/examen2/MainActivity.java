package com.example.examen2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


public class MainActivity extends AppCompatActivity{

    BottomNavigationView bottomNavigationView;
    //private Toolbar toolbar;
    FragmentTransaction transaction;
    Fragment fragmente1,mapsFragment;
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmente1 = new Fragment1();
        mapsFragment = new MapsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.contedorFragments,fragmente1).commit();

        bottomNavigationView = findViewById(R.id.btnNavigation);
        frameLayout = findViewById(R.id.contedorFragments);

        //setFragment(mapsFragment);

        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.item_ubicacion:
                        setFragment(mapsFragment);
                        break;
                    case R.id.item_avion:
                        setFragment(fragmente1);
                        break;
                }
            }
        });

    }
    private void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contedorFragments,fragment);
        fragmentTransaction.commit();
    }

}